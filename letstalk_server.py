from __future__ import absolute_import, division, print_function, unicode_literals
from keras.preprocessing import sequence
from keras.datasets import imdb
from keras import layers, models
from keras.models import Sequential
from keras import layers
import sys
import pickle
import numpy as np
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.utils import to_categorical
import random
from keras import optimizers
from keras.layers import SimpleRNN, Dense
from keras.layers import Bidirectional
import tensorflow as tf
from numpy import argmax
import atexit
import os
import re
import threading
from concurrent.futures import thread
from copy import deepcopy
from queue import Queue
from signal import signal, SIGINT

from flask import Flask, flash, request, redirect, url_for, json, Response
from werkzeug.utils import secure_filename
from flask_sqlalchemy import SQLAlchemy
from settings import *
from lmst import *
from videosplit import *
from util import constants as cs
from util.utils import *


UPLOAD_FOLDER = 'temp_videos'
ALLOWED_EXTENSIONS = {'mp4', 'avi'}
app = Flask(__name__)

# app.config.from_object(os.environ['APP_SETTINGS'])
app.config.from_object(os.getenv("APP_SETTINGS"))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

dead_files = Queue()
END_OF_DATA = object()  # a unique sentinel value


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/letstalk/getISL/')
def get_isl_urls():
    from word_to_reference import get_references_from_words

    sentence = request.args.get('sentence')
    urls_arr = get_references_from_words(sentence.lower())
    return Response(json.dumps(urls_arr), mimetype='application/json')


@app.route('/letstalk/getText/', methods=['POST'])
def get_text():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)

        result = ""

        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            user_id = filename[filename.rfind('_') + 1: filename.rfind('.')]
            upload_dir = os.path.join(app.config['UPLOAD_FOLDER'], user_id)
            Path(upload_dir).mkdir(parents=True, exist_ok=True)
            file.save(os.path.join(upload_dir, filename))
            markers = request.args.get('markers')
            file_list = video_split(markers, filename, upload_dir)  # final segmented videos in order
            #PREPROCESSING

            cmd='GLOG_logtostderr=1 bazel-bin/mediapipe/examples/desktop/multi_hand_tracking/multi_hand_tracking_cpu \--calculator_graph_config_file=mediapipe/graphs/hand_tracking/multi_hand_tracking_desktop_live.pbtxt'
            output_data_path = cs.output_data_path
            inp_list = []
            for mp4list in file_list:

                inputfilen='   --input_video_path='+mp4list
                outputfilen='   --output_video_path='+output_data_path
                cmdret=cmd+inputfilen+outputfilen
                outputnew = output_data_path
                nfilename = mp4list.split('/')[-1]
                nfilename = nfilename.split('.')[0]
                outputnew = outputnew + nfilename + '.txt'
                inp_list.append(outputnew)
                os.system(cmdret)

            #PREDICTION

            output_dir=output_data_path
            x_test=load_data(inp_list)
            new_model = tf.keras.models.load_model(cs.MODEL_SAVE)

            labels=load_label()

            xhat = x_test
            yhat = new_model.predict(xhat)
            predictions = np.array([np.argmax(pred) for pred in yhat])
            for i in predictions:

                result += labels[i]
                result += ' '

            # Delete temp files asynchronously
            dead_files.put(os.path.join(upload_dir, filename))
            for cut_file in file_list:
                dead_files.put(cut_file)
            for cut_file in inp_list:
                dead_files.put(cut_file)
        return_txt = {  # Return sentence in this dict. Replace "HELLO WORLD" with final string
            "text": result
        }
        return json.dumps(return_txt)


# dead files consumer
def background_deleter():
    while True:
        path = dead_files.get()
        if path is END_OF_DATA:
            return
        try:
            print('deleted {}'.format(path))
            os.remove(path)
        except:
            print('Error')
            pass


# close deleter thread
def close_running_threads():
    dead_files.put(END_OF_DATA)
    deleter.join()


# Handle Ctrl-C forced shutdown
def sigint_handler(sig, frame):
    close_running_threads()
    sys.exit(0)


if __name__ == "__main__":
    # Uncomment below 4 lines before deploying
    # deleter = threading.Thread(target=background_deleter)
    # deleter.start()
    # atexit.register(close_running_threads)  # register callback at shutdown of server
    # signal(SIGINT, sigint_handler)  # register same callback when server forced to shutdown using Ctrl-C
    app.run(host='0.0.0.0')
