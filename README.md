**LetsTalk - Backend**

Contains the source code for the backend of the LetsTalk Application built for the WinVinaya Sign Language project.

---

## Libraries used:

The main languages used are Python and C++(only for mediapipe). The libraries used are:

1. flask - Framework used for building the Rest API
2. nltk - Used for Natural Language Processing
3. tensorflow, keras - Deep Learning libraries used for implementing model
4. mediapipe - An open-source library used for hand-coordinate extraction.

---

## Structure

Essentially, all of the files except for those in 'extras' folder will be used in the deployment server.

---

## Extras

Contains the source code for creation, processing of dataset and the training of model. Also contains code for unit testing of utility functions that we did for essentially checking the correctness of our written code.

Only the saved model files obtained after training will be used in the server. For additional info on this folder, check out the 'readme.MD' file inside it.

---