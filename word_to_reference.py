import re

from models import WordVideoReference
from lmst import *


def get_references_from_words(sentence):
    tokens = nltk.word_tokenize(sentence)
    words = nltk.pos_tag(tokens)

    ref_arr = []
    for word, pos in words:
        if word == 'i':  # If the word is I we need to add it
            word_ref = WordVideoReference.query.filter_by(word='I').first()
            ref_arr.append(word_ref.serialize())
            continue

        # Use this to find number of words with same spelling
        # word_ref2 = WordVideoReference.query.filter(WordVideoReference.word.op('~')(word)).all()
        # if(len(word_ref2) > 1):
        #     word = resolve_context(word,tokens,pos)

        # To prevent losing data due to lemmatization
        word_ref = WordVideoReference.query.filter_by(word=word).first()
        if word_ref is not None and len(word) > 1:
            ref_arr.append(word_ref.serialize())
        else:  # If word is not present, then we lemmatize it
            word = lemmatize_word(word, pos)
            if word is None:
                continue

            # Use this to find number of words with same spelling
            word = re.escape(word)
            word_ref2 = WordVideoReference.query.filter(
                WordVideoReference.word.op('~')('{}_|{}%'.format(word, word))).all()
            if (len(word_ref2) > 1):  # If more than 1 word exist
                word = resolve_context(word, tokens, pos)  # Get correct context

            word_ref = WordVideoReference.query.filter_by(word=word).first()
            if word_ref is not None:
                ref_arr.append(word_ref.serialize())
            else:  # Now we decompose the word into letters
                for letter in word:
                    if letter == '%':
                        break
                    word_ref = WordVideoReference.query.filter_by(word=letter).first()
                    if word_ref is not None:
                        ref_arr.append(word_ref.serialize())

    return ref_arr
