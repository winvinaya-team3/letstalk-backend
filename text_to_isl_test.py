import unittest
import letstalk_server
import json

BASE_URL = 'http://127.0.0.1:5000/letstalk/'
GET_ISL_URL = '{}getISL/'.format(BASE_URL)


class TestTextToISL(unittest.TestCase):

    def setUp(self):
        self.app = letstalk_server.app.test_client()
        self.app.testing = True

    def test_text_to_isl(self):
        response = self.app.get(GET_ISL_URL, query_string={'sentence': 'how are you'})
        data = json.loads(response.get_data())
        self.assertEqual(str(data),
                         "[{'reference': 'how.mp4', 'word': 'how'},"
                         " {'reference': 'you.mp4', 'word': 'you'}]")

        response = self.app.get(GET_ISL_URL, query_string={'sentence': 'How are you ?'})
        data = json.loads(response.get_data())
        self.assertEqual(str(data),
                         "[{'reference': 'how.mp4', 'word': 'how'},"
                         " {'reference': 'you.mp4', 'word': 'you'},"
                         " {'reference': '?.mp4', 'word': '?'}]")

        response = self.app.get(GET_ISL_URL, query_string={'sentence': 'The bat flew away'})
        data = json.loads(response.get_data())
        self.assertEqual(str(data),
                         "[{'reference': 'animal_bat.mp4', 'word': 'cricket_bat%n%01'},"
                         " {'reference': 'f.mp4', 'word': 'f'},"
                         " {'reference': 'l.mp4', 'word': 'l'},"
                         " {'reference': 'y.mp4', 'word': 'y'}]")

        response = self.app.get(GET_ISL_URL, query_string={'sentence': 'I hit with the bat'})
        data = json.loads(response.get_data())
        self.assertEqual(str(data),
                         "[{'reference': 'I.mp4', 'word': 'I'},"
                         " {'reference': 'h.mp4', 'word': 'h'},"
                         " {'reference': 'i.mp4', 'word': 'i'},"
                         " {'reference': 't.mp4', 'word': 't'},"
                         " {'reference': 'with.mp4', 'word': 'with'},"
                         " {'reference': 'cricket_bat.mp4', 'word': 'squash_racket%n%01'}]")

        response = self.app.get(GET_ISL_URL, query_string={'sentence': 'I am going home'})
        data = json.loads(response.get_data())
        self.assertEqual(str(data),
                         "[{'reference': 'I.mp4', 'word': 'I'},"
                         " {'reference': 'go.mp4', 'word': 'go'},"
                         " {'reference': 'h.mp4', 'word': 'h'},"
                         " {'reference': 'o.mp4', 'word': 'o'},"
                         " {'reference': 'm.mp4', 'word': 'm'},"
                         " {'reference': 'e.mp4', 'word': 'e'}]")

        response = self.app.get(GET_ISL_URL, query_string={'sentence': 'She is beautiful'})
        data = json.loads(response.get_data())
        self.assertEqual(str(data),
                         "[{'reference': 's.mp4', 'word': 's'},"
                         " {'reference': 'h.mp4', 'word': 'h'},"
                         " {'reference': 'e.mp4', 'word': 'e'},"
                         " {'reference': 'b.mp4', 'word': 'b'},"
                         " {'reference': 'e.mp4', 'word': 'e'},"
                         " {'reference': 'a.mp4', 'word': 'a'},"
                         " {'reference': 'u.mp4', 'word': 'u'},"
                         " {'reference': 't.mp4', 'word': 't'},"
                         " {'reference': 'y.mp4', 'word': 'y'}]")


if __name__ == "__main__":
    unittest.main()
