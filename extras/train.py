from __future__ import absolute_import, division, print_function, unicode_literals
import os
import sys
from keras.preprocessing import sequence
from keras.datasets import imdb
from keras import layers, models
from keras.models import Sequential
from keras import layers
import numpy as np
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.utils import to_categorical
import random
from keras import optimizers
from keras.layers import SimpleRNN, Dense
from keras.layers import Bidirectional
from util.utils import make_label, load_data, build_data

input_train_path = 'train/'
epochs = 150
MODEL_SAVE = 'dump.h5'
FRAME_CNT = 70
LANDMARK_COUNT = 84  # 21 landmarks per frame per hand per co-ordinate, in total (21*4) per frame


def build_model(label):
    model = Sequential()
    model.add(layers.LSTM(64, return_sequences=True,
                   input_shape=(FRAME_CNT, LANDMARK_COUNT))) 
    model.add(layers.LSTM(32, return_sequences=True))
    model.add(layers.LSTM(32))
    model.add(layers.Dense(label, activation='softmax'))
    model.compile(loss='categorical_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])
    return model

def main():
    x_train,y_train=load_data(input_train_path)
    x_train,y_train= build_data(x_train,y_train)
    model=build_model(y_train.shape[1])
    print('Training stage')
    print('==============')
    history=model.fit(x_train,y_train,epochs=epochs,batch_size=16,validation_data=(x_train,y_train))
    score, acc = model.evaluate(x_train,y_train,batch_size=16,verbose=0)
    print('Test performance: accuracy={0}, loss={1}'.format(acc, score))
    model.save(MODEL_SAVE)

    
if __name__=='__main__':
    main()
