import os
import sys
from util import constants as cs
    
input_data_path = cs.input_data_path
output_data_path = cs.output_data_path
cmd='GLOG_logtostderr=1 bazel-bin/mediapipe/examples/desktop/multi_hand_tracking/multi_hand_tracking_cpu \--calculator_graph_config_file=mediapipe/graphs/hand_tracking/multi_hand_tracking_desktop_live.pbtxt'
listfile=os.listdir(input_data_path)
for file in listfile:
    if not(os.path.isdir(input_data_path+file)):
        continue
    word = file+"/"
    fullfilename=os.listdir(input_data_path+word)
    if not(os.path.isdir(output_data_path+word)):
        os.mkdir(output_data_path+word)
    for mp4list in fullfilename:
        if ".DS_Store" in mp4list:
            continue         
        inputfilen='   --input_video_path='+input_data_path+word+mp4list
        outputfilen='   --output_video_path='+output_data_path
        cmdret=cmd+inputfilen+outputfilen
        os.system(cmdret)
