**Backend - LetsTalk App [EXTRAS FOLDER]**

Contains the source code for training the model being used for classification.

To build your own dataset, copy 'mediapipe' and 'third_party' folders from project root directory into the extras directory.

Use these commands below to build mediapipe using bazel on Ubuntu 18.04.

##SETUP BAZEL

- sudo apt install curl gnupg
- curl https://bazel.build/bazel-release.pub.gpg | sudo apt-key add -
- echo "deb [arch=amd64] https://storage.googleapis.com/bazel-apt stable jdk1.8" | sudo tee /etc/apt/sources.list.d/bazel.list
- sudo apt update
- sudo apt install bazel

##SETUP OPENCV LIB
- sudo apt-get install libopencv-core-dev libopencv-highgui-dev libopencv-calib3d-dev libopencv-features2d-dev libopencv-imgproc-dev libopencv-video-dev

##BUILD BAZEL
- bazel build -c opt --define MEDIAPIPE_DISABLE_GPU=1 \mediapipe/examples/desktop/multi_hand_tracking:multi_hand_tracking_cpu

#build.py

- python3 build.py

Extract features(hand co-ordinates) from dataset videos. Put your videos in 'input' with videos of same gesture in same folders.

Eg:

```shell
input
---- Clap
	---- ClapVideo1
	---- ClapVideo2

---- Move
	---- MoveVideo1
	...
    
```

Your features will be extracted in 'output' as txt files with same folder hierarchy as in 'input'.

#train.py

- python3 train.py

Trains the LSTM model and saves the trained model in 'dump.h5' file. Labels are saved in 'label.txt'. Takes videos by default from 'output' folder.

#predict.py

- python3 predict.py

Checks for accuracy of trained model against files in 'processed' folder. For your custom dataset, move testing txt files taking care of hierarchy in 'processsed'.

#runtests.py

- python3 runtests.py

Unit tests written in python unittest library to check correctness of utility functions used.