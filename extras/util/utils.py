from __future__ import absolute_import, division, print_function, unicode_literals
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.utils import to_categorical
from . import constants as cs
import os
import sys
import numpy as np
import random

FRAME_CNT = cs.FRAME_CNT
LANDMARK_COUNT = cs.LANDMARK_COUNT  # 21 landmarks per frame per hand per co-ordinate, in total (21*4) per frame

def make_label(filename,text):
    with open(filename, "w") as f:
         f.write(text)
    f.close()

def load_label(filename):
    listfile=[]
    with open(filename,mode='r') as l:
        listfile=[i for i in l.read().split()]
    label = {}
    count = 1
    for l in listfile:
        if "_" in l:
            continue
        label[count] = l
        count += 1
    return label

def load_data(dirname):
    if dirname[-1]!='/':
        dirname=dirname+'/'
    listfile=os.listdir(dirname)
    X = []
    Y = []
    for file in listfile:
        if "_" in file:
            continue
        wordname=file
        textlist=os.listdir(dirname+wordname)
        for text in textlist:
            if "DS_" in text:
                continue
            textname=dirname+wordname+"/"+text
            numbers=[]
            #print(textname)
            with open(textname, mode = 'r') as t:
                numbers = [float(num) for num in t.read().split()]
                for i in range(len(numbers),15000):
                    numbers.extend([0.0]) 
            row=0
            landmark_frame=[]
            for i in range(0,FRAME_CNT):
                landmark_frame.extend(numbers[row:row+LANDMARK_COUNT])
                row += LANDMARK_COUNT
            landmark_frame=np.array(landmark_frame)
            landmark_frame=list(landmark_frame.reshape(-1,LANDMARK_COUNT))
            X.append(np.array(landmark_frame))
            Y.append(wordname)
            
    X=np.array(X)
    Y=np.array(Y)

    return X,Y

def build_data(X,Y):
    
    tmp = [[x,y] for x, y in zip(X, Y)]
    random.shuffle(tmp)
    
    X = [n[0] for n in tmp]
    Y = [n[1] for n in tmp]
    
    k=set(Y)
    ks=sorted(k)
    text=""
    for i in ks:
        text=text+i+" "
    make_label(cs.LABEL_SAVE, text)
    
    s = Tokenizer()
    s.fit_on_texts([text])
    encoded=s.texts_to_sequences([Y])[0]
    one_hot = to_categorical(encoded)    

    (x_train, y_train) = X, one_hot
    x_train=np.array(x_train)
    y_train=np.array(y_train)
    return x_train,y_train