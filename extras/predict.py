from __future__ import absolute_import, division, print_function, unicode_literals
from keras.preprocessing import sequence
from keras.datasets import imdb
from keras import layers, models
from keras.models import Sequential
from keras import layers
import os
import sys
import pickle
import numpy as np
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.utils import to_categorical
import random
from keras import optimizers
from keras.layers import SimpleRNN, Dense
from keras.layers import Bidirectional
import tensorflow as tf
from numpy import argmax
from util.utils import load_data,load_label
from util import constants as cs

input_predict_path = cs.input_predict_path
epochs = cs.epochs
MODEL_SAVE = cs.MODEL_SAVE
    
def main():

    output_dir=input_predict_path
    x_test,Y=load_data(output_dir)
    new_model = tf.keras.models.load_model(MODEL_SAVE)

    labels=load_label(cs.LABEL_SAVE)

    xhat = x_test
    yhat = new_model.predict(xhat)
    print(yhat[1])
    predictions = np.array([np.argmax(pred) for pred in yhat])
    s,sc=0,0
    for i in predictions:
        print("Prediction #{0} -- Expected: {1} ; Found: {2}".format(s,Y[s],labels[i]))
        if Y[s]==labels[i]:
            sc+=1
        s+=1
    print("Accuracy:",sc/s)
        

if __name__ == "__main__":
    main()
