import unittest
import os
import numpy as np
from util.utils import *
from util import constants as cs


class TestModel(unittest.TestCase):

	def test_createLoadLabels(self):

		temp_file = 'unittests/eXfhgw.txt'

		tests = ['The quick brown fox jumps over the lazy dog'
				,'Inception'
				]

		for test in tests:

			if os.path.exists(temp_file):
				os.remove(temp_file)

			make_label(temp_file, test)
			dict1 = load_label(temp_file)

			dict2 = {}
			cnt = 1
			for inp in test.split():

				dict2[cnt] = inp
				cnt+=1

			self.assertEqual(dict1,dict2)

		if os.path.exists(temp_file):
			os.remove(temp_file)

	def test_createFeatures(self):

		X = []
		Y = []

		testdir = 'unittests/testinput/'

		tmp = [2,3,4,5]
		for i in range(len(tmp),15000):
			tmp.extend([0.0])

		row = 0
		ntmp = []
		for i in range(0,FRAME_CNT):
			ntmp.extend(tmp[row:row+cs.LANDMARK_COUNT])
			row += cs.LANDMARK_COUNT

		ntmp=np.array(ntmp)
		ntmp=list(ntmp.reshape(-1,LANDMARK_COUNT))
		X.append(np.array(ntmp))
		Y.append('test1')

		X2, Y2 = load_data(testdir)
		X = np.array(X)
		Y = np.array(Y)

		self.assertTrue((X==X2).all())
		self.assertTrue((Y==Y2).all())


if __name__ == '__main__':
    unittest.main()