from flask import json

from letstalk_server import db


class WordVideoReference(db.Model):
    __tablename__ = 'video_urls'

    word = db.Column(db.String(), primary_key=True)
    reference = db.Column(db.String())

    def __init__(self, word, reference):
        self.word = word
        self.reference = reference

    def __repr__(self):
        video_urls_obj = {
            'word': self.word,
            'reference': self.reference
        }
        return json.dumps(video_urls_obj)

    @staticmethod
    def add_word(_word, _reference):
        new_word = WordVideoReference(word=_word, reference=_reference)
        db.session.add(new_word)
        db.session.commit()

    @staticmethod
    def get_all():
        return WordVideoReference.query.all()

    def serialize(self):
        return {
            'word': self.word,
            'reference': self.reference
        }
