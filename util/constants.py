# =====================
# Global Variables
# =====================

FRAME_CNT = 70
LANDMARK_COUNT = 84  # 21 landmarks per frame per hand per co-ordinate, in total (21*4) per frame
epochs = 150


# =====================
# Relative Paths
# =====================

input_data_path = 'input/'
output_data_path = 'output/'
input_train_path = 'train/'
input_predict_path = 'processed/'

# ===========================
# Saved Model File Names
# ===========================

MODEL_SAVE = 'dump.h5'